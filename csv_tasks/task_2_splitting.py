from utils import csv_to_list, list_to_csv

def split_csv(in_file_name:str, column_name:str, values:[str]):
    """Splits csv accoring to the given values in the given column"""
    [header, rows] = csv_to_list(in_file_name)
    column_no = header.index(column_name)
    groups = [] # nested data in [[header, [data]], [header, [data]]] format
    for index, value in enumerate(values):
        groups.append([])
        groups[index].append(header)
        groups[index].append([])
        for row in rows:
            if row[column_no] == value:
                groups[index][1].append(row)
    return groups

def save_split(groups:[], out_file_names:[str]):
    for index, group in enumerate(groups):
        list_to_csv(group, out_file_names[index])

values = ['male', 'female']
split = split_csv('./train.csv', 'Sex', values)
save_split(split, [f'./csv_tasks/output_files/{name}.csv' for name in values])

