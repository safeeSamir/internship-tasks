from utils import csv_to_list, list_to_csv
from typing import Callable

def select(in_file_name:str, column_name:str, check:Callable, selected_columns=None):
    """
    selects data from the given file that returns true when check is called
    in_file_name(str): Name of input file
    column_name(str): Name of the column for which check is called
    check(function): a function that takes a single value and returns a boolean
    selected_columns(List of str)(optional): When specified selects the given columns in the given order from the list
    """
    
    [header, rows] = csv_to_list(in_file_name)
    column_no = header.index(column_name)
    filtered = []
    if(selected_columns == None):
        selected_columns = header
    selected_indices = [header.index(selected_column) for selected_column in selected_columns]
    filtered.append(selected_columns)
    filtered.append([])
    for index, row in enumerate(rows):
        if check(row[column_no]):
            filtered[1].append([row[header.index(selected_column)] for selected_column in selected_columns])
    return filtered


def check_age_empty(age):
    return age != ''

def check_age_20(age):
    try:
        age = float(age)
        return age > 20
    except:
        return False
        
non_empty_age = select('./train.csv', 'Age', check_age_empty)
twenty_plus_age = select('./train.csv', 'Age', check_age_20, ['Name', 'Age', 'Sex', 'Fare'])

list_to_csv(non_empty_age, './csv_tasks/output_files/data_age.csv')
list_to_csv(twenty_plus_age, './csv_tasks/output_files/data_age_20_plus.csv')