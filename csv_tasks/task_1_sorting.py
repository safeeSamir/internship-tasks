from utils import csv_to_list, list_to_csv


def sort_by_column(data: [], column_name: str, ascending=True):
    [header, rows] = data
    column_no = header.index(column_name)
    # basic insertion sort
    for i in range(len(rows)):
        for j in range(i + 1, len(rows)):
            if rows[j][column_no] < rows[i][column_no]:
                # swap the entire rows
                rows[i], rows[j] = rows[j], rows[i]
    if(not ascending):
        rows.reverse()
    return header, rows


def sort_data(in_file_name: str, out_file_name: str, column_name: str, ascending=True):
    data = csv_to_list(in_file_name)
    sort_by_column(data, column_name, ascending)
    list_to_csv(data, out_file_name)


sort_data('train.csv', './csv_tasks/output_files/sorted.csv', 'Fare')
