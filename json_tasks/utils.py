"""Separate utility functions that are used in every task"""
import csv
import json

def csv_to_dicts(path: str):
    """
    converts csv to list of dicts similar to json format
    """
    with open(path, 'r', encoding="utf-8") as file:
        reader = csv.reader(file)
        header = next(reader)
        rows = []
        for row in reader:
            obj = {}
            for index, column in enumerate(row):
                obj[header[index]] = column
            rows.append(obj)
        return rows

def save_as_json(data: [], path: str):
    """
    saves the data as json with the given file name
    """
    with open(path, 'w', encoding="utf-8") as file:
        as_json = json.dumps(data)
        file.write(as_json)