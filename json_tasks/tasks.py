from utils import csv_to_dicts, save_as_json

data = csv_to_dicts('train.csv')
min_fare = float(data[0]["Fare"])
max_fare = float(data[0]["Fare"])
min_age = float(data[0]["Age"])
max_age = float(data[0]["Age"])
sum_age = 0
average_age = 0
count = 0
non_empty_age_rows = 0
embarked = {}

for obj in data:
    if obj["Fare"] != '':
        min_fare = min(min_fare, float(obj["Fare"]))
        max_fare = max(max_fare, float(obj["Fare"]))
    if obj["Age"] != '':
        min_age = min(min_age, float(obj["Age"]))
        max_age = max(max_age, float(obj["Age"]))
        non_empty_age_rows += 1
        sum_age += float(obj["Age"])
    if obj["Embarked"] in embarked.keys():
        embarked[obj["Embarked"]] += 1
    else:
        embarked[obj["Embarked"]] = 0
    count += 1

average_age = sum_age / non_empty_age_rows
result = {
    "minFare": min_fare,
    "maxFare": max_fare,
    "minAge": min_age,
    "maxAge": max_age,
    "averageAge": average_age,
    "totalRows" : len(data[0].keys()),
    "totalColumns": count,
    "embarked": embarked
}
save_as_json(result, "./json_tasks/result.json")



    
    