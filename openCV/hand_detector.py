import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
import time
target = cv.imread('./openCV/hand.jpg')
cap = cv.VideoCapture(0)
orb = cv.ORB_create()
matcher = cv.BFMatcher()
if cap.isOpened():
    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            cap.release()
        target_kps, target_des = orb.detectAndCompute(target,None)
        input_kps, input_des = orb.detectAndCompute(frame,None)
        matches = matcher.match(target_des,input_des)
        good = []
        for m in matches:
            if m.distance < 300:
                good.append(m)
        if len(good)>10:
            src_pts = np.float32([ target_kps[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
            dst_pts = np.float32([ input_kps[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

            M, mask = cv.findHomography(src_pts, dst_pts, cv.RANSAC,5.0)
            matchesMask = mask.ravel().tolist()
            h,w,channel = target.shape
            pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
            dst = cv.perspectiveTransform(pts,M)
            frame = cv.polylines(frame,[np.int32(dst)],True,255,3, cv.LINE_AA)
            
        cv.imshow('frame', frame)
        if cv.waitKey(1) == ord('q'):
            cap.release()
        #gray = cv.cvtColor(frame, cv.COLOR_BGR2BGRA)
        
    cv.destroyAllWindows()