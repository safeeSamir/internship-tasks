"""Separate utility functions that are used in every task"""
import csv


def csv_to_list(path: str):
    """
    converts csv to [header, [data]] format
    """
    with open(path, 'r', encoding="utf-8") as file:
        reader = csv.reader(file)
        header = next(reader)
        rows = []
        for row in reader:
            rows.append(row)
        return [header, rows]


def list_to_csv(data: [], path: str):
    """
    converts from [header, [data]] to csv format
    """
    with open(path, 'w', encoding="utf-8") as file:
        writer = csv.writer(file)
        [header, rows] = data
        writer.writerow(header)
        writer.writerows(rows)
