from typing import Tuple
import numpy as np
from scipy.signal import correlate2d, convolve2d

class Convolution:
    def __init__(self, input_shape: Tuple, kernel_size: int, output_depth: int):
        self.input = None
        self.output = None
        input_depth, input_width, input_height = input_shape
        self.input_shape: Tuple = input_shape
        self.input_depth: int = input_depth
        self.output_depth: int = output_depth
        self.output_shape: Tuple = (
            self.output_depth,
            input_height - kernel_size + 1,
            input_width - kernel_size + 1
        )
        self.kernels_shape: Tuple = (
            output_depth, input_depth,
            kernel_size, kernel_size
        )
        self.kernels: np.array = np.random.randn(*self.kernels_shape)
        self.biases: np.array = np.random.randn(*self.output_shape)

    def forward(self, input: np.array):
        self.input: np.array = input
        self.output: np.array = np.copy(self.biases)
        for i in range(self.output_depth):
            for j in range(self.input_depth):
                self.output[i] += correlate2d(self.input[j],
                                              self.kernels[i, j], 'valid')
        # print("Conv Forward:", self.output)
        return self.output

    def backward(self, output_gradient, learning_rate):
        kernel_gradients: np.array = np.zeros(self.kernels_shape)
        input_gradients: np.array = np.zeros(self.input_shape)
        for i in range(self.output_depth):
            for j in range(self.input_depth):
                kernel_gradients[i, j] = correlate2d(
                    self.input[j], output_gradient[i], 'valid')
                input_gradients[j] += convolve2d(
                    output_gradient[i], self.kernels[i, j], 'full')
        self.kernels -= learning_rate * kernel_gradients
        self.biases -= learning_rate * output_gradient
        # print("Conv Backward:", input_gradients)
        return input_gradients


class Reshape:
    def __init__(self, input_shape: Tuple, output_shape: Tuple):
        self.input_shape: Tuple = input_shape
        self.output_shape: Tuple = output_shape

    def forward(self, input: np.array):
        # print("Reshape Forward:", np.reshape(input, self.output_shape))
        return np.reshape(input, self.output_shape)

    def backward(self, output_gradient, learning_rate):
        # print("Reshape Backward:", np.reshape(output_gradient, self.input_shape))
        return np.reshape(output_gradient, self.input_shape)


class Dense:
    def __init__(self, input_size: int, output_size: int):
        self.input = None
        self.output = None
        self.weights = np.random.randn(output_size, input_size)
        self.biases = np.random.randn(output_size, 1)
        

    def forward(self, input: np.array):
        self.input = input
        # print("Dense Forward", np.dot(self.weights, self.input) + self.biases)
        return np.dot(self.weights, self.input) + self.biases

    def backward(self, output_gradient, learning_rate):
        weights_gradient = np.dot(output_gradient, self.input.T)
        self.weights -= learning_rate * weights_gradient
        self.biases -= learning_rate * output_gradient
        # print("Dense Backward", np.dot(self.weights.T, output_gradient))
        return np.dot(self.weights.T, output_gradient)
