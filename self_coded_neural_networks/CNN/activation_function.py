import numpy as np


class Sigmoid:
    def forward(self, x):
        self.input = x
        return 1 / (1 + np.exp(-x))

    def backward(self, output_gradient, learning_rate):
        s = self.forward(self.input)
        return np.multiply(output_gradient, s * (1 - s))

class Softmax:
    def forward(self, x):
        x = np.clip(x, 1e-7, 1 - 1e-7)
        tmp = np.exp(x)
        self.output = tmp / np.sum(tmp)
        # print("Softmax Forward: ", self.output)
        return self.output

    def backward(self, output_gradient, learning_rate):
        n = np.size(self.output)
        tmp = np.tile(self.output, n)
        # print("Softmax Backward: ", np.dot(tmp * (np.identity(n) - np.transpose(n)), output_gradient))
        return np.multiply(output_gradient, np.dot(tmp * (np.identity(n) - np.transpose(n)), output_gradient))

