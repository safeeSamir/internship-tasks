import numpy as np

class CategoricalCrossEntropy:
    def forward(self, actual: np.array, predicted: np.array):
        predicted = np.clip(predicted, 1e-7, 1 - 1e-7)
        index = np.where(actual == 1)[0][0]
        # print("Loss Forward: ", -np.log(predicted[index]))
        return -np.log(predicted[index])

    def backward(self, actual: np.array, predicted: np.array):
        predicted = np.clip(predicted, 1e-7, 1 - 1e-7)
        return -actual/predicted

class BinaryCrossEntropy:
    def forward(self, y_true: np.array, y_pred: np.array):
        y_pred = np.clip(y_pred, 1e-7, 1 - 1e-7)
        return np.mean(-y_true * np.log(y_pred) - (1 - y_true) * np.log(1 - y_pred))

    def backward(self, y_true: np.array, y_pred: np.array):
        return ((1 - y_true) / (1 - y_pred) - y_true / y_pred) / np.size(y_true)