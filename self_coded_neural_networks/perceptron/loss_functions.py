
def difference(output: float, correct_value: float):
    return correct_value - output

def squared_error(output: float, correct_value: float):
    return (correct_value - output) * (correct_value - output)