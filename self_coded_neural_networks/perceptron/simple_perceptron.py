import numpy as np
from typing import Callable

class Perceptron:
    def __init__(self, input_size:int, activation_function:Callable, loss_function:Callable, epochs=10, learning_rate=0.01):
        self.input_size = input_size
        self.weights = 0.01 * np.random.randn(input_size)
        self.bias = np.float64(0)
        self.activation_function = activation_function
        self.loss_function = loss_function
        self.epochs = epochs
        self.learning_rate = learning_rate
    
    def predict(self, X:np.array):
        print(self.weights)
        print(X)
        dot = self.weights.T.dot(X) + self.bias
        return self.activation_function(dot)
    
    def train(self, training_set:np.array, output_column:int):
        training = training_set.copy()

        for i in range(self.epochs):
            for example in training:
                output = example.pop(output_column)
                predicted = self.predict(np.array(example))
                loss = self.loss_function(predicted, output)
                self.update(X[i], e)

    def update(self, pred_value, loss):
        self.weights += self.learning_rate * loss * pred_value
        self.bias += self.learning_rate * loss
            

